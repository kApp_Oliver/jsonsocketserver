import XCTest

import JSONSocketServerTests

var tests = [XCTestCaseEntry]()
tests += JSONSocketServerTests.allTests()
XCTMain(tests)
