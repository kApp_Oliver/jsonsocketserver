import XCTest
import SSLService
@testable import JSONSocketServer

final class JSONSocketServerTests: XCTestCase, JSONSocketServerClientDelegate {
    
    var server : JSONSocketServer?
    
    func testExample() {
        do {
            self.server = try JSONSocketServer(port: 8080, logLevel: .verbose,sslConfiguration: nil,socketType: .tcp)
            server!.clientDelegate = self
            while server!.state != .stopped {
                print("Keep Alive")
                print("Connections - \(self.server?.connectedClients.count)")
                for (_, client) in server!.connectedClients {
                   // client.send(payload: ["Hello": 1])
                }
                sleep(10)
            }
            XCTAssert(true)
        }catch (let e){
            print(e.localizedDescription)
            XCTAssert(false)
        }
        
    }
    
    func welcomeMessage(for client: JSONSocketServerClient) -> JSONSocketServer.PayloadType? {
        return ["message" : "Welcome!!","boundary" : client.boundary]
    }
    
    func received(message: JSONSocketServer.PayloadType, for client: JSONSocketServerClient) {
        print(message)
        if let request = message["request"] as? String {
            switch request {
            case "echo":
                client.send(payload: ["question" : message["message"] as Any]) { message in
                    print("We have a response...")
                    print(client.responseQueue.count)
                }
            case "close":
                client.close()
                break;
            case "shutdown":
                server?.stop()
                break;
            default:
                break;
            }
        }else{
            sleep(5)
            client.send(payload: ["message" : "Hello!", "origin" : message])
        }
    }
    
    func clientDisconnected(clientId: UUID) {
        
    }
    
    func clientConnected(client: JSONSocketServerClient) {
        
    }
    
    static var allTests = [
        ("testExample", testExample),
    ]
}
