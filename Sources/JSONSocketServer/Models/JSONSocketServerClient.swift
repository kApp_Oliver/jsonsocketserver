//
//  JSONSocketServerClient.swift
//  
//
//  Created by Oliver Bates on 23/10/2019.
//

import Foundation
import Socket

public protocol JSONSocketServerClientDelegate {
    func welcomeMessage(for client: JSONSocketServerClient) -> JSONSocketServer.PayloadType?
    func received(message: JSONSocketServer.PayloadType,for client: JSONSocketServerClient)
    func clientDisconnected(clientId: UUID)
    func clientConnected(client: JSONSocketServerClient)
}
extension JSONSocketServerClientDelegate {
    func welcomeMessage(for client: JSONSocketServerClient) -> JSONSocketServer.PayloadType? {
        return ["boundary" : client.boundary]
    }
    func received(message: JSONSocketServer.PayloadType,for client: JSONSocketServerClient){
        
    }
    func clientDisconnected(clientId: UUID){
        
    }
    func clientConnected(client: JSONSocketServerClient){
        
    }
}

public class JSONSocketServerClient {
    public let id = UUID()
    public var socket : Socket?
    var buffer = Data()
    public let boundary : String = UUID().uuidString
    var messageQueue = DispatchQueue(label: "uk.kAppuccino.JSONSocketServer.messageQueue", qos: .background)
    var socketQueue = DispatchQueue(label: "uk.kAppuccino.JSONSocketServer.socketQueue", qos: .background)
    var delegate : JSONSocketServerClientDelegate?
    var server : JSONSocketServer?
    var socketType : JSONSocketServer.SocketType = .tcp
    var udpAddress : Socket.Address?
    var responseQueue = [String : ((message: JSONSocketServer.PayloadType)->Void)]()
    
    public convenience init(withSocket socket: Socket) {
        self.init()
        self.socket = socket
    }
    
    func log(_ message: String){
        if server?.logLevel == .verbose {
            print("JSONSocketServerClient [\(id.uuidString)] > \(message)")
        }
    }
    
    public func send(payload: JSONSocketServer.PayloadType,withBoundary : Bool = true,then completionHandler: ((_ message: JSONSocketServer.PayloadType)->Void)? = nil){
        var payload = payload //Make Mutable...
        if let completionHandler = completionHandler {
            let requestId = UUID().uuidString
            payload["requestId"] = requestId
            self.responseQueue[requestId] = { message in
                self.responseQueue[requestId] = nil
                completionHandler(message)
            }
        }
        DispatchQueue.global().async { [unowned self,socket] in
            if self.socketType == .tcp {
                do {
                    guard try socket?.isReadableOrWritable().writable == true else {
                        return
                    }
                }catch let e {
                    print(e.localizedDescription)
                    return
                }
                guard socket?.isConnected == true else {
                    return
                }
            }
            
            guard let data = try? JSONSerialization.data(withJSONObject: payload, options: .init()) else {
                return
            }
            guard self.socketType == .tcp else {
                if withBoundary {
                    let _ = try? socket?.write(from: self.boundary, to: self.udpAddress!)
                }
                let _ = try? socket?.write(from: data, to: self.udpAddress!)
                return
            }
            if withBoundary {
                let _ = try? socket?.write(from: self.boundary)
            }
            let _ = try? socket?.write(from: data)
        }
    }
    
    public func close(){
        if socketType == .tcp {
            socket?.close()
            log("Socket Closed - \(id.uuidString)")
        }else {
            log("UDP Client Closed - \(id.uuidString)")
        }
        delegate?.clientDisconnected(clientId: id)
        server?.connectedClients[id.uuidString] = nil
        if let udpAddress = udpAddress {
            if let udpDecodedAddress = Socket.hostnameAndPort(from: udpAddress) {
                server?.datagramClientMap["\(udpDecodedAddress.hostname)@\(udpDecodedAddress.port)"] = nil
            }
        }
    }
    
    
    func decodedBuffer() -> String? {
        guard let utf8Value = String(data: buffer, encoding: .utf8) else {
            log("Warning - No UTF8 Buffer Detected.")
            guard let asciiValue = String(data: buffer, encoding: .ascii) else {
                log("Error - Not UTF8. Not ASCII. Invalid character in buffer.")
                return nil
            }
            guard let utfConversion = asciiValue.data(using: .utf8) else {
                log("Error - Not UTF8. Is ASCII. UTF8 Conversion Failed.")
                return nil
            }
            guard let utfConversionString = String(data: utfConversion, encoding: .utf8) else {
                log("Error - Not UTF8. Is ASCII. UTF8 String Conversion Failed.")
                return nil
            }
            log("Successfully converted ASCII to UTF8")
            return utfConversionString
        }
        return utf8Value
    }
    
    func handleBuffer(){
        guard let bufferValue = decodedBuffer() else {
            close()
            return
        }
        var messages : [Data] = [buffer]
        let components = bufferValue.components(separatedBy: boundary)
        messages = []
        for component in components {
            var component = component
            if component.hasSuffix("\n") {
                component.removeLast(1)
            }
            guard let componentData = component.data(using: .utf8) else {
                continue
            }
            messages.append(componentData)
        }
        var validBuffer = false
        for messageData in messages {
            if let payloadInstance = try? JSONSerialization.jsonObject(with: messageData, options: .allowFragments) as? JSONSocketServer.PayloadType {
                if let requestId = payloadInstance["requestId"] as? String {
                    self.responseQueue[requestId]?(payloadInstance)
                }else {
                    delegate?.received(message: payloadInstance, for: self)
                }
                validBuffer = true
            }
        }
        if validBuffer {
            buffer.removeAll()
        }
    }
    
    func handle(){
        guard socket?.isConnected == true else {
            log("Ending handle for closed socket.")
            return
        }
        do {
            let readWriteable = try socket?.isReadableOrWritable(waitForever: false, timeout: 100)
            guard readWriteable?.readable == true else {
                usleep(200000) //SLEEP FOR 200ms
                socketQueue.async {
                    self.handle()
                }
                return
            }
            let result = try socket?.read(into: &buffer)
            if result == 0 {
                //CLOSE SOCKET...
                close()
                return
            }
            guard let bufferValue = decodedBuffer() else {
                close()
                return
            }
            var messages : [Data] = [buffer]
            let components = bufferValue.components(separatedBy: boundary)
            messages = []
            for component in components {
                guard let componentData = component.data(using: .utf8) else {
                    continue
                }
                messages.append(componentData)
            }
            var validBuffer = false
            for messageData in messages {
                if let payloadInstance = try? JSONSerialization.jsonObject(with: messageData, options: .allowFragments) as? JSONSocketServer.PayloadType {
                    if let requestId = payloadInstance["requestId"] as? String {
                        self.responseQueue[requestId]?(payloadInstance)
                    }else {
                        delegate?.received(message: payloadInstance, for: self)
                    }
                    validBuffer = true
                }
            }
            if validBuffer {
                buffer.removeAll()
            }
            handleBuffer()
            
        }catch (let e){
            log("Error caught - \(e.localizedDescription)")
            close()
            return
        }
        handle()
    }
    
    func start(){
        if let welcomeMessage = delegate?.welcomeMessage(for: self) {
            send(payload: welcomeMessage, withBoundary: false)
        }else{
            send(payload: ["boundary" : boundary])
        }
        guard socketType == .tcp else {
            return
        }
        socketQueue.async {
            self.handle()
        }
    }
}
