//
//  JSONPayloadCoders.swift
//  
//
//  Created by Oliver Bates on 23/10/2019.
//

import Foundation

public class JSONPayloadEncoder<T : Encodable> {
    var model : T
    public init(_ model: T){
        self.model = model
    }
    public func encode() -> JSONSocketServer.PayloadType? {
        guard let encoded = try? JSONEncoder().encode(model) else {
            return nil
        }
        guard let decodedDictionary = try? JSONSerialization.jsonObject(with: encoded, options: .allowFragments) as? JSONSocketServer.PayloadType else {
            return nil
        }
        return decodedDictionary
    }
}
public class JSONPayloadDecoder<T : Decodable> {
    var dictionary : JSONSocketServer.PayloadType
    var decodeType : T.Type
    public init(_ dictionary: JSONSocketServer.PayloadType,type decodeType: T.Type){
        self.dictionary = dictionary
        self.decodeType = decodeType
    }
    public func decode() -> T? {
        guard let encoded = try? JSONSerialization.data(withJSONObject: dictionary, options: .init()) else {
            return nil
        }
        guard let decoded = try? JSONDecoder().decode(decodeType, from: encoded) else {
            return nil
        }
        return decoded
    }
}
