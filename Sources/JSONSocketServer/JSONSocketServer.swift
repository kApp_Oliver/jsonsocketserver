import Foundation
import Socket
import SSLService

public class JSONSocketServer {
    //Version No
    public static let version = 1.0
    
    public enum SocketType {
        case udp
        case tcp
    }
    
    public enum NotificationName : String {
        case didChangeState
    }
    public enum LogLevel {
        case none
        case verbose
    }
    public enum State {
        case running
        case stop
        case stopped
    }
    
    public typealias PayloadType = [String : Any]
    var listenSocket : Socket?
    public var logLevel : LogLevel = .none
    public var socketType : SocketType = .tcp
    var state : State = .running {
        didSet {
            NotificationCenter.default.post(name: Notification.Name(NotificationName.didChangeState.rawValue), object: self)
        }
    }
    public var connectedClients = [String : JSONSocketServerClient]()
    public var clientDelegate : JSONSocketServerClientDelegate?
    var datagramClientMap = [String : String]()
    
    public init(port: Int,logLevel : LogLevel = .none,sslConfiguration: SSLService.Configuration? = nil,socketType: SocketType = .tcp) throws{
        self.logLevel = logLevel
        self.socketType = socketType
        if socketType == .tcp {
            listenSocket = try Socket.create()
        }else{
            listenSocket = try Socket.create(family: .inet, type: .datagram, proto: .udp)
        }
        if let sslConfiguration = sslConfiguration {
            log("Enabling SSL")
            listenSocket?.delegate = try SSLService(usingConfiguration: sslConfiguration)
            log("SSL Enabled")
        }
        if socketType == .tcp {
            try listenSocket?.listen(on: port)
            DispatchQueue.global().async {
                try? self.listenForConnections()
            }
        }else{
            //try listenSocket?.udpBroadcast(enable: true)
            DispatchQueue.global().async {
                try? self.listenForDatagrams(port)
            }
        }
    }
    
    func log(_ message: String){
        if logLevel == .verbose {
            print("JSONSocketServer > \(message)")
        }
    }
    
    public func stop(){
        state = .stop
        DispatchQueue.global().async {
            for (_, client) in self.connectedClients{
                client.socket?.close()
            }
            self.connectedClients.removeAll()
            self.listenSocket?.close()
            self.state = .stopped
        }
    }
    
    public func waitUntilStopped(){
        while state != .stopped {
            sleep(10)
        }
    }
    
    func listenForDatagrams(_ port: Int) throws {
        var datagramBuffer = Data()
        log("Listening for datagram...")
        if let datagramMeta = try listenSocket?.listen(forMessage: &datagramBuffer, on: port) {
            log("New datagram - \(datagramMeta.bytesRead) bytes")
            if let datagramAddress = datagramMeta.address {
                //CHECK FOR EXISTING CLIENT...
                if let decodedAddress = Socket.hostnameAndPort(from: datagramAddress) {
                    let clientUDPId = "\(decodedAddress.hostname)@\(decodedAddress.port)"
                    print(clientUDPId)
                    guard let existingClientId = datagramClientMap[clientUDPId] else {
                        //CREATE NEW CLIENT
                        log("Accepted new UDP connection.")
                        let newClient = JSONSocketServerClient(withSocket: listenSocket!)
                        newClient.delegate = clientDelegate
                        newClient.server = self
                        newClient.socketType = .udp
                        newClient.udpAddress = datagramAddress
                        connectedClients[newClient.id.uuidString] = newClient
                        datagramClientMap[clientUDPId] = newClient.id.uuidString
                        clientDelegate?.clientConnected(client: newClient)
                        newClient.start()
                        newClient.buffer.append(datagramBuffer)
                        newClient.handleBuffer()
                        try listenForDatagrams(port)
                        return
                    }
                    guard let existingClient = connectedClients[existingClientId] else {
                        //THIS SHOULD NEVER HAPPEN...
                        //CREATE NEW CLIENT
                        log("Accepted new UDP connection.")
                        let newClient = JSONSocketServerClient(withSocket: listenSocket!)
                        newClient.delegate = clientDelegate
                        newClient.server = self
                        newClient.socketType = .udp
                        newClient.udpAddress = datagramAddress
                        connectedClients[newClient.id.uuidString] = newClient
                        datagramClientMap[clientUDPId] = newClient.id.uuidString
                        clientDelegate?.clientConnected(client: newClient)
                        newClient.start()
                        newClient.buffer.append(datagramBuffer)
                        newClient.handleBuffer()
                        try listenForDatagrams(port)
                        return
                    }
                    existingClient.buffer.append(datagramBuffer)
                    existingClient.handleBuffer()
                    try listenForDatagrams(port)
                }
            }
        }
        log("Failed to accept datagram.")
    }
    
    func listenForConnections() throws {
        guard state == .running else {
            log("Listener shut down.")
            return
        }
        log("Listening for connections...")
        if let newSocket = try listenSocket?.acceptClientConnection(invokeDelegate: true) {
            log("Accepted new connection.")
            let newClient = JSONSocketServerClient(withSocket: newSocket)
            newClient.delegate = clientDelegate
            newClient.server = self
            connectedClients[newClient.id.uuidString] = newClient
            clientDelegate?.clientConnected(client: newClient)
            DispatchQueue.global().async {
                newClient.start()
            }
        } else {
            log("Connection accepted then dropped.")
        }
        
        try listenForConnections()
    }
}
